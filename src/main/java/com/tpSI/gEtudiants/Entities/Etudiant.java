package com.tpSI.gEtudiants.Entities;

import lombok.*;
import javax.persistence.Entity;
import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
//@RequiredArgsConstructor
@Data
@Entity
@Table(name = "Etudiant")
public class Etudiant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long code;

    private boolean supprimeEtudiant=false;
}
