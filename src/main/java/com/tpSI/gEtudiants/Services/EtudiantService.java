package com.tpSI.gEtudiants.Services;

import com.tpSI.gEtudiants.Entities.Etudiant;

import java.util.List;

public interface EtudiantService {
//    public Etudiant create ();
    Etudiant saveEtudiant(Etudiant etudiant);
    Etudiant findEtudiant(Long idEtudiant);
    Etudiant updateEtudiant(Long idEtudiant,Etudiant etudiant);
    Etudiant deleteEtudiant(Long idEtudiant);
    List<Etudiant> listEtudiant();
    List<Etudiant> listEtudiantNonSupprime();
}
