package com.tpSI.gEtudiants.Controllers;

import com.tpSI.gEtudiants.Entities.Etudiant;
import com.tpSI.gEtudiants.Repositories.EtudiantRepository;
import com.tpSI.gEtudiants.Services.EtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/etudiant")
@CrossOrigin("*")
public class EtudiantController {
    private final RestTemplate restTemplate;
    @Autowired
    public EtudiantController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Autowired
    private EtudiantService etudiantService;

    @Autowired
    private EtudiantRepository etudiantRepository;

    @PostMapping("/save")
    public Etudiant saveEtudiant(@RequestBody Etudiant etudiant) {
        return etudiantService.saveEtudiant(etudiant);
    }
    @GetMapping("/list")
    public List<Etudiant> listEtudiant(){
        return etudiantService.listEtudiant();
    }
    @GetMapping("/list/nonSupprime")
    public List<Etudiant> listEtudiantNonSupprime(){
        return etudiantService.listEtudiantNonSupprime();
    }
    @GetMapping("{idEtudiant}")
    public Etudiant findEtudiant(@PathVariable("idEtudiant")Long idEtudiant){
        return etudiantService.findEtudiant(idEtudiant);
    }
    @PutMapping("update/{idEtudiant}")
    public Etudiant updateEtudiant(@RequestBody Etudiant etudiant,@PathVariable("idEtudiant") Long idEtudiant){
        return etudiantService.updateEtudiant(idEtudiant,etudiant);
    }
    @PutMapping("delete/{idEtudiant}")
    public Etudiant deleteEtudiant(@PathVariable("idEtudiant") Long idEtudiant){
        return etudiantService.deleteEtudiant(idEtudiant);
    }

    @DeleteMapping("delete/def/{idEtudiant}")
    public String deletedefEtudiant(@PathVariable("idEtudiant") Long idEtudiant){
        etudiantRepository.deleteById(idEtudiant);
        return "Personne supprime";
    }
}
