package com.tpSI.gEtudiants.ServiceImplementations;

import com.tpSI.gEtudiants.Entities.Etudiant;
import com.tpSI.gEtudiants.Repositories.EtudiantRepository;
import com.tpSI.gEtudiants.Services.EtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EtudiantImplementation implements EtudiantService {
    @Autowired
    EtudiantRepository etudiantRepository;
    @Override
    public Etudiant saveEtudiant(Etudiant etudiant) {
        return etudiantRepository.save(etudiant);
    }

    @Override
    public Etudiant findEtudiant(Long idEtudiant) {
        return etudiantRepository.findById(idEtudiant).get();
    }

    @Override
    public Etudiant updateEtudiant(Long idEtudiant, Etudiant etudiant) {
        return null;
    }

    @Override
    public Etudiant deleteEtudiant(Long idEtudiant) {
        Etudiant etudiant1 = etudiantRepository.findById(idEtudiant).get();
        etudiant1.setSupprimeEtudiant(true);
        return etudiantRepository.save(etudiant1);
    }

    @Override
    public List<Etudiant> listEtudiant() {
        return etudiantRepository.findAll();
    }

    @Override
    public List<Etudiant> listEtudiantNonSupprime() {
        return null;
    }
}
